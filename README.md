Open in an IDE or another similar program to see correct whitespace.

Clock variables can be made three different ways. One way is to not fill any values in. This will create a clock with zero filled in for all values, and it will output in 24 hour time.
The second way is to fill in the inputs as (int hours, int minutes, int seconds). This will fill in the respective values. The final way is to fill in all the inputs as (int hours, int minutes, int seconds, bool twelveHourTime).

Here is the outputs of three Clock objects created as shown above:
Clock 1: 0:0:0
Clock 2: 1:2:3
Clock 3: 9:9:9 AM

If the hour is zero, and the output is set to twelve hour time, the hour will be read out as 12 as expected:
12:0:0 AM

If the hour is above 12, and the output is set to twelve hour time, the hour will be read out as PM:
3:0:0 PM

12/24 hour time can be switched between as shown below:
clock set to 24 hour time
15:0:0
clock set to 12 hour time
3:0:0 PM

Hours, minutes, and seconds can all be added to the clocks. Here is the output when 5 hours, 5 minutes, and 5 seconds is added to a clock that is initially 0:0:0:
5:5:5

Hours, minutes, and seconds can all be subtracted from the clocks. Here is the output when 1 hours, 2 minutes, and 3 seconds are subtracted from the above clock:
4:3:2

Here are the results of the comparison operands being used on Clock 1 and Clock 2 shown above:
Clock 1 == Clock 2:
  Expected: 0, got: 0
Clock 1 != Clock 2:
  Expected: 1, got: 1
Clock 1 > Clock 2:
  Expected: 0, got: 0
Clock 1 < Clock 2:
  Expected: 1, got: 1
Clock 1 >= Clock 2:
  Expected: 0, got: 0
Clock 1 <= Clock 2:
  Expected: 1, got: 1

2 hours have been added to the value of Clock 1. The output is now:
2:0:0
For reference, Clock 2 is:
1:2:3

Here is the results of the comparison operands run again on Clock  and Clock 2
Clock 1 == Clock 2:
  Expected: 0, got: 0
Clock 1 != Clock 2:
  Expected: 1, got: 1
Clock 1 > Clock 2:
  Expected: 1, got: 1
Clock 1 < Clock 2:
  Expected: 0, got: 0
Clock 1 >= Clock 2:
  Expected: 1, got: 1
Clock 1 <= Clock 2:
  Expected: 0, got: 0

Here is proof the == and != operator work:
0:0:0 == 0:0:0:
  Expected: 1, got: 1
0:0:0 != 0:0:0:
  Expected: 0, got: 0

Two clocks can be added together. Here is the result of Clock 1 and Clock 2 being added together (the 12/24 ourput time is preserved from the left-most clock):
3:2:3

Subtraction also works:
0:57:57
As shown above: hours, minutes, and seconds behave as expected when times are subtracted.

If the time is ever set to be negative (hour is negative), 24 hours is added until the hours is positive. This is to simulate showing the time of the previous day. An announcement will announce when this happens.
Here is an example of an hour being subtracted from a clock reading 0:0:0:
Negative hours detected. 24 hours have been added.
23:0:0

The clocks also behave as expected when the hours/minutes/seconds are set to values higher than those shown on a normal clock.
This means that when hours is set to above 24, the printed hours will hours % 24. Minutes and seconds behave similarly.
Here is the output of a clock with the time set as 100:100:100:
5:41:40
