#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "Clock.h"

int main() {
    cout << "Clock variables can be made three different ways. One way is to not fill any values in. This will create a clock with zero filled in for all values, and it will output in 24 hour time.\n";
    cout << "The second way is to fill in the inputs as (int hours, int minutes, int seconds). This will fill in the respective values. The final way is to fill in all the inputs as (int hours, int minutes, int seconds, bool twelveHourTime).\n";
    Clock c1;
    Clock c2(1,2,3);
    Clock c3(9,9,9,true);
    bool tmpBool;

    cout << "\nHere is the outputs of three Clock objects created as shown above:\n";
    cout << "Clock 1: ";
    c1.Print();
    cout << "Clock 2: ";
    c2.Print();
    cout << "Clock 3: ";
    c3.Print();
    
    cout << "\nIf the hour is zero, and the output is set to twelve hour time, the hour will be read out as 12 as expected:\n";
    Clock zeroClock(0,0,0,true);
    zeroClock.Print();
    cout << "\nIf the hour is above 12, and the output is set to twelve hour time, the hour will be read out as PM:\n";
    Clock pmClock(15,0,0,true);
    pmClock.Print();
    cout <<"\n12/24 hour time can be switched between as shown below:\n";
    pmClock.TwelveHour(false);
    pmClock.Print();
    pmClock.TwelveHour(true);
    pmClock.Print();


    cout << "\nHours, minutes, and seconds can all be added to the clocks. Here is the output when 5 hours, 5 minutes, and 5 seconds is added to a clock that is initially 0:0:0:\n";
    Clock addClock;
    addClock.AddHours(5);
    addClock.AddMinutes(5);
    addClock.AddSeconds(5);
    addClock.Print();

    cout << "\nHours, minutes, and seconds can all be subtracted from the clocks. Here is the output when 1 hours, 2 minutes, and 3 seconds are subtracted from the above clock:\n";
    addClock.SubtractHours(1);
    addClock.SubtractMinutes(2);
    addClock.SubtractSeconds(3);
    addClock.Print();

    cout << "\nHere are the results of the comparison operands being used on Clock 1 and Clock 2 shown above:\n";
    tmpBool = c1 == c2;
    cout << "Clock 1 == Clock 2:\n  Expected: 0, got: " << tmpBool << endl;
    tmpBool = c1 != c2;
    cout << "Clock 1 != Clock 2:\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = c1 > c2;
    cout << "Clock 1 > Clock 2:\n  Expected: 0, got: " << tmpBool << endl;
    tmpBool = c1 < c2;
    cout << "Clock 1 < Clock 2:\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = c1 >= c2;
    cout << "Clock 1 >= Clock 2:\n  Expected: 0, got: " << tmpBool << endl;
    tmpBool = c1 <= c2;
    cout << "Clock 1 <= Clock 2:\n  Expected: 1, got: " << tmpBool << endl;

    cout << "\n2 hours have been added to the value of Clock 1. The output is now:\n";
    c1.AddHours(2);
    c1.Print();
    cout << "For reference, Clock 2 is:\n";
    c2.Print();

    cout << "\nHere is the results of the comparison operands run again on Clock  and Clock 2\n";
    tmpBool = c1 == c2;
    cout << "Clock 1 == Clock 2:\n  Expected: 0, got: " << tmpBool << endl;
    tmpBool = c1 != c2;
    cout << "Clock 1 != Clock 2:\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = c1 > c2;
    cout << "Clock 1 > Clock 2:\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = c1 < c2;
    cout << "Clock 1 < Clock 2:\n  Expected: 0, got: " << tmpBool << endl;
    tmpBool = c1 >= c2;
    cout << "Clock 1 >= Clock 2:\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = c1 <= c2;
    cout << "Clock 1 <= Clock 2:\n  Expected: 0, got: " << tmpBool << endl;

    cout << "\nHere is proof the == and != operator work:\n";
    Clock tmpClock1;
    Clock tmpClock2;
    tmpBool = tmpClock1 == tmpClock2;
    cout << "0:0:0 == 0:0:0:";
    cout << "\n  Expected: 1, got: " << tmpBool << endl;
    tmpBool = tmpClock1 != tmpClock2;
    cout << "0:0:0 != 0:0:0:";
    cout << "\n  Expected: 0, got: " << tmpBool << endl;

    cout << "\nTwo clocks can be added together. Here is the result of Clock 1 and Clock 2 being added together (the 12/24 ourput time is preserved from the left-most clock):\n";
    Clock mathClock;
    mathClock = c1 + c2;
    mathClock.Print();

    cout << "\nSubtraction also works:\n";
    mathClock = c1 - c2;
    mathClock.Print();
    cout << "As shown above: hours, minutes, and seconds behave as expected when times are subtracted.\n";

    cout << "\nIf the time is ever set to be negative (hour is negative), 24 hours is added until the hours is positive. This is to simulate showing the time of the previous day. An announcement will announce when this happens.\n";
    cout << "Here is an example of an hour being subtracted from a clock reading 0:0:0:\n";
    Clock negativeClock;
    negativeClock.SubtractHours(1);
    negativeClock.Print();

    cout << "\nThe clocks also behave as expected when the hours/minutes/seconds are set to values higher than those shown on a normal clock.\n";
    cout << "This means that when hours is set to above 24, the printed hours will hours % 24. Minutes and seconds behave similarly.\n";
    cout << "Here is the output of a clock with the time set as 100:100:100:\n";
    Clock bigClock(100,100,100);
    bigClock.Print();
    return 0;
}