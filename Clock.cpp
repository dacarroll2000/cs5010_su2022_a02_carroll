#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "Clock.h"

Clock::Clock() {
   //default constructor
   hours = 0;
   minutes = 0;
   seconds = 0;
   twelveHourTime = false;
}

Clock::Clock(int hrs, int min, int sec) {
   //constructor
   hours = hrs;
   minutes = min;
   seconds = sec;
   twelveHourTime = false;

   while (seconds < 0) {
      SubtractMinutes(1);
      seconds += 60;
   }
   while(minutes < 0) {
      SubtractHours(1);
      minutes += 60;
   }
   while (hours < 0) {
   cout << "Negative hours detected. 24 hours have been added." << endl;
   hours += 24;
   }
}

Clock::Clock(int hrs, int min, int sec, bool usrPreference) {
   //constructor
   hours = hrs;
   minutes = min;
   seconds = sec;
   twelveHourTime = usrPreference;

   while (seconds < 0) {
      SubtractMinutes(1);
      seconds += 60;
   }
   while(minutes < 0) {
      SubtractHours(1);
      minutes += 60;
   }
   while (hours < 0) {
   cout << "Negative hours detected. 24 hours have been added." << endl;
   hours += 24;
   }
}

void Clock::Print() const {
   //these are the values that will be printed
   int prntHours = 0;
   int prntMinutes = 0;
   int prntSeconds = 0;

   //temporary values for converting seconds to minutes, etc.
   int tmpMinutes = 0;
   int tmpHours = 0;

   //calculating what numbers to print
   prntSeconds = seconds % 60;

   tmpMinutes = minutes + (seconds / 60);
   prntMinutes = (minutes + (seconds / 60)) % 60;

   prntHours = hours + (tmpMinutes / 60);

   //logic to determine how to output the clock
   if (twelveHourTime) {
      if ((hours % 24) > 11){
         cout << prntHours % 12 << ":" << prntMinutes << ":" << prntSeconds << " PM" <<  endl;
      }
      else {
         if ((hours % 24) == 0) {
            cout <<"12:" << prntMinutes << ":" << prntSeconds << " AM"  << endl;
         }
         else {
            cout << prntHours % 12 << ":" << prntMinutes << ":" << prntSeconds << " AM" << endl;
         }
      }
   }
   else {
      cout << prntHours % 24 << ":" << prntMinutes << ":" << prntSeconds << endl;
   }
}

void Clock::TwelveHour(bool usrPreference) {
   //set 12/24 hour preference
   twelveHourTime = usrPreference;
   if (usrPreference) {
      cout << "clock set to 12 hour time" << endl;
   }
   else {
      cout << "clock set to 24 hour time" << endl;
   }
}

void Clock::AddHours(int hrs) {
   //adds hours
   hours += hrs;
}

void Clock::AddMinutes(int min) {
   //adds minutes
   minutes += min;
}

void Clock::AddSeconds(int sec) {
   //add seconds
   seconds += sec;
}

void Clock::SubtractHours(int hrs) {
   //subtract hours
   hours -= hrs;
   //check and correction for negative hours
   while (hours < 0) {
      cout << "Negative hours detected. 24 hours have been added." << endl;
      hours += 24;
   }
}

void Clock::SubtractMinutes(int min) {
   //subtract minutes
   minutes -= min;
   //check and correction for negative minutes
   while(minutes < 0) {
      SubtractHours(1);
      minutes += 60;
   }
}

void Clock::SubtractSeconds(int sec) {
   //subtract seconds
   seconds -= sec;
   //check and correction for negative seconds
   while (seconds < 0) {
      SubtractMinutes(1);
      seconds += 60;
   }
}

Clock Clock::operator+(const Clock& rhs) const {
   //temporary clock for easy math
   Clock tmpClock(hours, minutes, seconds, twelveHourTime);
   tmpClock.AddSeconds(rhs.seconds);
   tmpClock.AddMinutes(rhs.minutes);
   tmpClock.AddHours(rhs.hours);
   return tmpClock;
}

Clock Clock::operator-(const Clock& rhs) const {
   //temporary clock for easy math
   Clock tmpClock(hours, minutes, seconds, twelveHourTime);
   tmpClock.SubtractSeconds(rhs.seconds);
   tmpClock.SubtractMinutes(rhs.minutes);
   tmpClock.SubtractHours(rhs.hours);
   return tmpClock;
}

bool Clock::operator==(const Clock& rhs) const {
   return (hours == rhs.hours && minutes == rhs.minutes && seconds == rhs.seconds);
}

bool Clock::operator!=(const Clock& rhs) const {
   //temporary clock for easy comparison
   Clock tmpClock(hours, minutes, seconds);
   return !(tmpClock == rhs);
}

bool Clock::operator<(const Clock& rhs) const {
   if (hours < rhs.hours) {
      return true;
   }
   if ((hours == rhs.hours) && (minutes < rhs.minutes)) {
      return true;
   }
   if ((hours == rhs.hours) && (seconds < rhs.seconds)) {
      return true;
   }
   return false;
}

bool Clock::operator>(const Clock& rhs) const {
   //temporary clock for easy comparison
   Clock tmpClock(hours, minutes, seconds);
   return (rhs < tmpClock);
}

bool Clock::operator<=(const Clock& rhs) const {
   //temporary clock for easy comparison
   Clock tmpClock(hours, minutes, seconds);
   return !(tmpClock > rhs);
}

bool Clock::operator>=(const Clock& rhs) const {
   //temporary clock for easy comparison
   Clock tmpClock(hours, minutes, seconds);
   return !(tmpClock < rhs);
}