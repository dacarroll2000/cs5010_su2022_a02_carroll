#ifndef CLOCK_H
#define CLOCK_H

class Clock {
   public:
      Clock();
      Clock(int hrs, int min, int sec);
      Clock(int hrs, int min, int sec, bool usrPreference);
      void SetTime(int hours, int minutes, int seconds);
      void Print() const;
      void TwelveHour(bool usrPreference);
      void AddHours(int hrs);
      void AddMinutes(int min);
      void AddSeconds(int sec);
      void SubtractHours(int hrs);
      void SubtractMinutes(int min);
      void SubtractSeconds(int sec);
      Clock operator+(const Clock& rhs) const;
      Clock operator-(const Clock& rhs) const;
      bool operator==(const Clock& rhs) const;
      bool operator!=(const Clock& rhs) const;
      bool operator<(const Clock& rhs) const;
      bool operator>(const Clock &rhs) const;
      bool operator<=(const Clock& rhs) const;
      bool operator>=(const Clock& rhs) const;

   private:
      int hours;
      int minutes;
      int seconds;
      bool twelveHourTime;

};

#endif